import { AppRoutes } from './app.router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { Compo1Component } from './compo1/compo1.component';
import { Component2Component } from './component2/component2.component';


@NgModule({
  declarations: [
    AppComponent,
    Compo1Component,
    Component2Component
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
