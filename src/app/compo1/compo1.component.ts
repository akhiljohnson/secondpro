import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})
export class Compo1Component implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }

  nextPage(){
    this.router.navigate(['compo2'])
  }
}
